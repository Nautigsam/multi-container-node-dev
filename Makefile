export CURRENT_UID=$(id -u):$(id -g)

js_dirs = monolith service

# Setup

install:
	@mkdir -p node_modules
	docker-compose -f docker-compose.builder.yml run --rm install


# Dev

start: genDevEnv
	docker-compose up
stop: genDevEnv
	docker-compose down
watch: genDevEnv
	docker-compose -f docker-compose.yml -f docker-compose.watch.yml up -d
debug: genDevEnv
	docker-compose -f docker-compose.yml -f docker-compose.debug.yml up -d
clean: stop
	@rm -rf node_modules
	@rm -rf conf/compose.env

testUnit: genTestEnv
	docker-compose -f docker-compose.builder.yml -f docker-compose.test.yml run --rm mocha ./**/test/unit $(arg)
testApi: genTestEnv
	docker-compose -f docker-compose.builder.yml -f docker-compose.test.yml run --rm mocha monolith/test/api $(arg)
valid: testUnit testApi

lint:
	docker-compose -f docker-compose.builder.yml run --rm eslint --fix $(js_dirs)
lintCheck:
	docker-compose -f docker-compose.builder.yml run --rm eslint $(js_dirs)

# Diagnostic

logs:
	docker-compose logs -f 2>/dev/null


# Add/remove packages
#
# Example: make add arg="lodash pino"

add:
	docker-compose -f docker-compose.builder.yml run --rm add $(arg)
rm:
	docker-compose -f docker-compose.builder.yml run --rm rm $(arg)


# Tools

genDevEnv:
	@cat conf/dev.env.in | envsubst > conf/compose.env
genTestEnv:
	@cat conf/test.env.in | envsubst > conf/compose.env