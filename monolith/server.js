"use strict";

const express = require("express");
const nats = require("nats");
const pino = require("pino");

const log = pino(pino.destination());

const app = express();

const { COMPOSE_MODE, SOME_VARIABLE } = process.env;

console.log(
  `ENV:\nCOMPOSE_MODE = "${COMPOSE_MODE}"\nSOME_VARIABLE = "${SOME_VARIABLE}"`
);

app.get("/", (req, res) => {
  log.error("This is an error");
  console.error("error in console");
  res.send('<a href="/hello">Say hello to service</a>');
});
app.get("/hello", (req, res) => {
  log.info("The user asked us to say hello to service");
  client.subscribe("hello-response", { max: 1 }, () => {
    log.info("The service responded!");
    res.send("The service responded!");
  });
  client.publish("hello", null, "hello-response");
});

const client = nats.connect("nats");
client.on("error", (err) => {
  log.error(err);
});
client.on("connect", () => {
  log.info("Connected to NATS");
  app.listen(3333, () => {
    log.info("Server started");
  });
});
