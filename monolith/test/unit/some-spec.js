"use strict";

describe("Monolith unit test", function () {
  it("should be ok", function () {
    "OK".should.equal("OK");
  });
  it("should be nok", function () {
    "NOK".should.equal("NOK");
  });
  it("TestEnv should be used", function () {
    process.env.SOME_VARIABLE.should.equal("monolith-test");
  });
});
