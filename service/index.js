"use strict";

const nats = require("nats");
const pino = require("pino");

const log = pino(pino.destination());

const client = nats.connect("nats");

client.subscribe("hello", (msg, replyTo) => {
  log.info("Monolith says hello, we say hello back");
  client.publish(replyTo);
});

client.on("error", (err) => {
  console.error(err);
});
client.on("connect", () => {
  log.info("Connected to NATS");
});
